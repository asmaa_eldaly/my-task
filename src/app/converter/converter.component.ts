import { Component, OnInit } from '@angular/core';
import { ConvertService } from './convert.service';

@Component({
  selector: 'app-converter',
  templateUrl: './converter.component.html',
  styleUrls: ['./converter.component.css']
})
export class ConverterComponent implements OnInit {
  calculatedValue = 0;
  allCurrencies = [];
  from : string;
  to : string;
  amount : string;

  constructor(
    private convertService : ConvertService
  ) { }

  ngOnInit() {
    this.convertService.convertCurrency(this.from, this.to,this.amount).subscribe(
      response=>{
        console.log(response);
      });


  }

}
