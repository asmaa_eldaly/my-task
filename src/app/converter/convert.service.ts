import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConvertService {
  apiUrl : string = 'http://data.fixer.io/api/convert?access_key=8a6fa06ff58c13389873b7d7a2b1afde';
  constructor(
    private http: HttpClient
  ) { }

  convertCurrency(from,to,amount){
    return this.http.get(this.apiUrl) + '&from=' + from + '&to=' + to + '&amount=' + amount;
  }

}
