import { TestBed } from '@angular/core/testing';

import { CurrencyRateDetailsService } from './currency-rate-details.service';

describe('CurrencyRateDetailsService', () => {
  let service: CurrencyRateDetailsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CurrencyRateDetailsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
