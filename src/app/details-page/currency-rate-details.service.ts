import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CurrencyRateDetailsService {
  apiUrl : string = 'http://data.fixer.io/api/latest?access_key=8a6fa06ff58c13389873b7d7a2b1afde';

  constructor(
    private http: HttpClient
  ) { }

  getAllRatesBaseEur(){
    return this.http.get(this.apiUrl);
  }
  getAllRatesBaseUsd(){
    return this.http.get(this.apiUrl + '& base = USD');
  }
}
