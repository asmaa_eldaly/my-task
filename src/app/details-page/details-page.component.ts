import { Component, OnInit } from '@angular/core';
import { CurrencyRateDetailsService } from './currency-rate-details.service';

@Component({
  selector: 'app-details-page',
  templateUrl: './details-page.component.html',
  styleUrls: ['./details-page.component.css']
})
export class DetailsPageComponent implements OnInit {

  constructor(
    private rateViewService: CurrencyRateDetailsService
  ) { }

  ngOnInit(){
    this.rateViewService.getAllRatesBaseEur().subscribe(response=>{
      console.log(response);
    });
    this.rateViewService.getAllRatesBaseUsd().subscribe( response=>{
      console.log(response);
    })
  }

}
